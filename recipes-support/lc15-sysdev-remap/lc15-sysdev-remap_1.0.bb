DESCRITOPN = "Remap all available system devices of Litecell15 platform as symbolic links to easy to reach place in /var/lc15/"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/BSD;md5=3775480a712fc46a69647678acb234cb"

inherit update-rc.d

SRC_URI = "file://lc15-sysdev-remap \
	   file://lc15-sysdev-remap.init \
	   file://lc15-sysdev-remap.service"

S = "${WORKDIR}"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "lc15-sysdev-remap_1.0.bb"
PR       := "r${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

RDEPENDS_${PN} += "backup-scripts"

do_install() {
	install -d ${D}${sysconfdir}
	install -d ${D}${sysconfdir}/systemd
	install -d ${D}${sysconfdir}/systemd/system
	install -d ${D}${sysconfdir}/systemd/system/multi-user.target.wants
	install -d ${D}${sysconfdir}/init.d
	install -m 0755 -d ${D}${base_libdir}
	install -m 0755 -d ${D}${systemd_unitdir}
	install -m 0755 -d ${D}${systemd_unitdir}/system
	install -d ${D}${bindir}
	install -d ${D}/var/volatile/lc15
	ln -sf volatile/lc15 ${D}/var/lc15
	install -m 0755 ${S}/lc15-sysdev-remap ${D}${bindir}/lc15-sysdev-remap
	install -m 0755 ${S}/lc15-sysdev-remap.init ${D}${sysconfdir}/init.d/lc15-sysdev-remap
	install -m 0644 ${S}/lc15-sysdev-remap.service ${D}${systemd_unitdir}/system/lc15-sysdev-remap.service
	ln -sf ${systemd_unitdir}/system/lc15-sysdev-remap.service  ${D}${sysconfdir}/systemd/system/multi-user.target.wants/lc15-sysdev-remap.service
}

FILES_${PN} += "${bindir} \
		${sysconfdir} \
		${systemd_unitdir} \
		${sysconfdir}/init.d \
		/var/lc15"

INSANE_SKIP_${PN} = "arch"
INITSCRIPT_PACKAGES = "${PN}"
INITSCRIPT_NAME_${PN} = "lc15-sysdev-remap"
INITSCRIPT_PARAMS_${PN} = "defaults 21 21"




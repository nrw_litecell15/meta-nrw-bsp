DESCRIPTION = "Setup time initially from ntp server, than use gps only after"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/BSD;md5=3775480a712fc46a69647678acb234cb"

inherit update-rc.d systemd pkgconfig

SRC_URI = "file://gettime.sh \
	   file://gettime.init \
	   file://ntp.conf \
	   file://gettime.conf \
	   file://gettime.service"

DEPENDS = "ntp"
RDEPENDS_${PN} += "ntp"

S = "${WORKDIR}"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "gettime.bb"
PR       := "r${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE_${PN} = "gettime.service"

RPROVIDES_${PN} += "${PN}-systemd"
RREPLACES_${PN} += "${PN}-systemd"
RCONFLICTS_${PN} += "${PN}-systemd"

do_install() {
	install -d ${D}${sysconfdir}
	install -d ${D}${sysconfdir}/init.d
	install -d ${D}${bindir}
	install -d ${D}${sysconfdir}/defaultconfig
	install -d ${D}${sysconfdir}/defaultconfig/config
	install -m 0755 -d ${D}${base_libdir}
	install -m 0755 -d ${D}${systemd_unitdir}
	install -m 0755 -d ${D}${systemd_unitdir}/system
	install -m 0755 ${S}/gettime.sh ${D}${bindir}/gettime.sh
	install -m 0755 ${S}/gettime.init ${D}${sysconfdir}/init.d/gettime
	install -m 0644 ${S}/ntp.conf ${D}${sysconfdir}/defaultconfig/config/ntp.conf
	install -m 0755 ${S}/gettime.conf ${D}${sysconfdir}/defaultconfig/config/gettime.conf
	ln -sf /mnt/rom/user/config/gettime.conf ${D}${sysconfdir}/gettime.conf
	install -m 0644 ${S}/gettime.service ${D}${systemd_unitdir}/system/gettime.service
}

FILES_${PN} += "${bindir} \
		${sysconfdir} \
		${sysconfdir}/init.d \
		${systemd_unitdir}/* \
        "

INSANE_SKIP_${PN} = "arch"
INITSCRIPT_PACKAGES = "${PN}"
INITSCRIPT_NAME_${PN} = "gettime"
INITSCRIPT_PARAMS_${PN} = "defaults 22 22"

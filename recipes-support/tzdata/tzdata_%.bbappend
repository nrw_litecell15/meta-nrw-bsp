inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "tzdata_%.bbappend"
PR       := "${PR}.${REPOGITFN}"

pkg_postinst_${PN}_append () {
        install -d "$D${sysconfdir}/defaultconfig"
        install -d "$D${sysconfdir}/defaultconfig/config"
	rm -f "$D${sysconfdir}/localtime"
	ln -sf /mnt/rom/user/config/localtime "$D${sysconfdir}/localtime"
	tz="Universal"
	ln -sf "${datadir}/zoneinfo/${tz}" "$D${sysconfdir}/defaultconfig/config/localtime"
}


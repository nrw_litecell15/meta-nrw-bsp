FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "ntp_%.bbappend"
PR       := "${PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

SYSTEMD_SERVICE_ntpdate =""

pkg_postinst_ntpdate() {
	exit 0
}

do_install_append() {
	rm -f ${D}${sysconfdir}/ntp.conf
	ln -sf /mnt/rom/user/config/ntp.conf ${D}${sysconfdir}/ntp.conf
}

FILES_${PN} += "${sysconfdir} \
		"


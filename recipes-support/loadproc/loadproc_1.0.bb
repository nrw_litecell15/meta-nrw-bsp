DESCRIPTION = "Load remoteproc processor"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/BSD;md5=3775480a712fc46a69647678acb234cb"

inherit pkgconfig

SRC_URI = "file://loadproc \
	   "

RDEPENDS_${PN} += "kernel-module-remoteproc kernel-module-omap-remoteproc kernel-module-nrw-mbox kernel-module-rtfifo kernel-module-msgqueue"

S = "${WORKDIR}"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "loadproc_1.0.bb"
PR       := "r${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

do_install() {
	install -d ${D}${bindir}
	install -m 0755 ${S}/loadproc ${D}${bindir}/loadproc
}

FILES_${PN} += "${bindir} \
        "

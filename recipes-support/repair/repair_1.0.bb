DESCRIPTION = "Check boot devices if they are healthy and repair them if not"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/BSD;md5=3775480a712fc46a69647678acb234cb"

inherit update-rc.d

SRC_URI = "file://checkboot.init \
	file://checkboot \
	file://checkboot.sh \
"

DEPENDS = "u-boot-tools"
RDEPENDS_${PN} += "u-boot-tools cronie busybox util-linux coreutils"

S = "${WORKDIR}"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "repair_1.0.bb"
PR       := "r${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

do_install() {
	install -d ${D}${sysconfdir}
	install -d ${D}${sysconfdir}/init.d
	install -m 0755 ${S}/checkboot.init ${D}${sysconfdir}/init.d/checkboot
	install -d ${D}${bindir}
	install -m 0755 ${S}/checkboot ${D}${bindir}/.checkboot
	install -m 0755 ${S}/checkboot.sh ${D}${bindir}/checkboot
}

pkg_postinst_${PN}_append() {
	echo "adding crontab"
	test -d $D/var/spool/cron || mkdir -p $D/var/spool/cron
	test -f /var/spool/cron/root && sed -i '/checkboot/d' $D/var/spool/cron/root
	echo "0 3 * * *    nice -n 15 ${bindir}/checkboot" >> $D/var/spool/cron/root
}

FILES_${PN} += "${sysconfdir} \
		${sysconfdir}/init.d \
		${bindir}"

INSANE_SKIP_${PN} = "arch"
INITSCRIPT_PACKAGES = "${PN}"
INITSCRIPT_NAME_${PN} = "checkboot"
INITSCRIPT_PARAMS_${PN} = "defaults 25 25"




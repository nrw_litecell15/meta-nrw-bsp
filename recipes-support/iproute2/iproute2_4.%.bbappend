FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRC_URI += "file://${PN}-shaping@${IF}.service \
	file://shaping \
	"
inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "iproute2_4.%.bbappend"
PR       := "${PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

RDEPENDS_${PN} += "bash"

#default Ethernet interface
IF = "eth0"

do_install_append () {
	#install directory
	install -d 0755 ${D}${sysconfdir}
	install -d 0755 ${D}${sysconfdir}/${PN}

	#install traffic shaping script 
	install -m 0755 ${WORKDIR}/shaping ${D}${sysconfdir}/${PN}
	
	#default configuration directory
	install -d 0755 ${D}${sysconfdir}/defaultconfig
	install -d 0755 ${D}${sysconfdir}/defaultconfig/config

	#move config in default config folder and replace by a symbolic link 
	mv ${D}${sysconfdir}/${PN} ${D}${sysconfdir}/defaultconfig/config/
	ln -sf /mnt/rom/user/config/${PN}/ ${D}${sysconfdir}/${PN}

	#install systemd service unit
	install -d ${D}${systemd_unitdir}/system/multi-user.target.wants/
	install -m 0644 ${WORKDIR}/${PN}-shaping@${IF}.service ${D}${systemd_unitdir}/system/

	# enable systemd on sysinit
	ln -sf ../${PN}-shaping@${IF}.service ${D}${systemd_unitdir}/system/multi-user.target.wants/
}

SYSTEMD_SERVICE_${PN} += "${PN}-shaping@${IF}.service"

FILES_${PN} += "${sysconfdir}/defaultconfig/config/${PN} \
		${systemd_unitdir} \
		"

CONFFILES_${PN} += "${sysconfdir}/defaultconfig/config/${PN}/shaping"

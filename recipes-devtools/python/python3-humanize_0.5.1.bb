DESCRIPTION = "Python humanize utilities"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI[md5sum] = "e8473d9dc1b220911cac2edd53b1d973"
SRC_URI[sha256sum] = "a43f57115831ac7c70de098e6ac46ac13be00d69abbf60bdcac251344785bb19"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "${PN}_${PV}.bb"
PR       := "r${REPOGITFN}"

inherit pypi setuptools3

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "opkg-arch-config_%.bbappend"
PR       := "${PR}.${REPOGITFN}"

do_install_append() {
	install -d 0755 ${D}${sysconfdir}
	install -d 0755 ${D}${sysconfdir}/defaultconfig
	install -d 0755 ${D}${sysconfdir}/defaultconfig/config
	install -d 0755 ${D}${sysconfdir}/defaultconfig/config/opkg
	install -d 0755 ${D}/mnt
	install -d 0755 ${D}/mnt/rom
	install -d 0755 ${D}/mnt/rom/user
	# Setup for runtime opkg configuration
	install -m 0644 ${D}${sysconfdir}/opkg/arch.conf ${D}${sysconfdir}/defaultconfig/config/opkg/arch.conf
	rm -rf ${D}${sysconfdir}/opkg/arch.conf
	ln -sf /mnt/rom/user/config/opkg/arch.conf ${D}${sysconfdir}/opkg/arch.conf
}

FILES_${PN} += " \
	${sysconfdir} \
	${sysconfdir}/defaultconfig \
	${sysconfdir}/defaultconfig/config \
	${sysconfdir}/defaultconfig/config/opkg/* \
	/mnt \
	/mnt/rom \
	/mnt/rom/user \
"

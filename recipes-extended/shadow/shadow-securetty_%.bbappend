inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "shadow-securetty_%.bbappend"
PR       := "${PR}.${REPOGITFN}"

do_install_append() {
	install -d ${D}${sysconfdir}/defaultconfig
	install -d ${D}${sysconfdir}/defaultconfig/config
	# Makes securetty user flash configurable, but empty by default to disallow serial root login
	echo -n > ${D}${sysconfdir}/securetty
	install -m 0400 ${D}${sysconfdir}/securetty ${D}${sysconfdir}/defaultconfig/config/securetty
	rm -rf ${D}${sysconfdir}/securetty
	ln -sf /mnt/rom/user/config/securetty ${D}${sysconfdir}/securetty
}

FILES_${PN} += "\
	${sysconfdir}/defaultconfig/config/securetty \
	"

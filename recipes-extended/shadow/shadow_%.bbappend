inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "shadow_%.bbappend"
PR       := "${PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

SRC_URI += "file://10-commonio_c-for-flash.patch \
           file://11-lockpw_c-for-flash.patch \
           file://12-grpunconv_c-for-flash.patch \
           file://13-pwunconv_c-for-flash.patch \
	   file://14-vipw_c-for-flash.patch \
	   "

FILESEXTRAPATHS_append := "${THISDIR}/files:"

do_install_append() {
	install -d ${D}${sysconfdir}/login-defs
	# Makes logins default user bindable (it must be there and not in flash to allow rootfs buildable on host)
	install -m 0644 ${D}${sysconfdir}/login.defs ${D}${sysconfdir}/login-defs/login.defs
	rm -rf ${D}${sysconfdir}/login.defs
	# symlink must be relative to allow it to work on host at rootfs build time
	ln -sf ../etc/login-defs/login.defs ${D}${sysconfdir}/login.defs
}

FILES_${PN}-base += "\
	${sysconfdir}/login-defs/login.defs \
	"

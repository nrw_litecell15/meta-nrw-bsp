inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "watchdog-config.bbappend"
PR       := "${PR}.${REPOGITFN}"

do_install_append() {
	install -d ${D}${sysconfdir}/defaultconfig
	install -d ${D}${sysconfdir}/defaultconfig/config
	cp ${D}${sysconfdir}/watchdog.conf ${D}${sysconfdir}/defaultconfig/config/watchdog.conf
	rm -f ${D}${sysconfdir}/watchdog.conf
	ln -sf /mnt/rom/user/config/watchdog.conf ${D}${sysconfdir}/watchdog.conf
	sed -i '/repair-timeout/d' ${D}${sysconfdir}/defaultconfig/config/watchdog.conf
	echo "repair-timeout		=240" >> ${D}${sysconfdir}/defaultconfig/config/watchdog.conf
	sed -i '/test-timeout/d' ${D}${sysconfdir}/defaultconfig/config/watchdog.conf
	echo "test-timeout		=300" >> ${D}${sysconfdir}/defaultconfig/config/watchdog.conf
	sed -i '/test-directory/d' ${D}${sysconfdir}/defaultconfig/config/watchdog.conf
	echo "test-directory=/mnt/rom/user/config/watchdog.d" >> ${D}${sysconfdir}/defaultconfig/config/watchdog.conf
	sed -i '/retry-timeout/d' ${D}${sysconfdir}/defaultconfig/config/watchdog.conf
	echo "retry-timeout = 0" >> ${D}${sysconfdir}/defaultconfig/config/watchdog.conf
	sed -i '/repair-maximum/d' ${D}${sysconfdir}/defaultconfig/config/watchdog.conf
	echo "repair-maximum = 0" >> ${D}${sysconfdir}/defaultconfig/config/watchdog.conf
}

FILES_${PN} += "${sysconfdir}/defaultconfig/config/* \
            "

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "watchdog_5.15.bbappend"
PR       := "${PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

SRC_URI = "${SOURCEFORGE_MIRROR}/watchdog/watchdog-${PV}.tar.gz \
           file://0001-Include-linux-param.h-for-EXEC_PAGESIZE-definition.patch \
           file://watchdog-init.patch \
           file://watchdog-conf.patch \
           file://wd_keepalive.init \
	   file://watchdog-init2.patch \
	   file://cksystemd.sh \
	   file://ckhwwdt.sh \
	   "

FILESEXTRAPATHS_append := "${THISDIR}/files:"
INITSCRIPT_PACKAGES = "${PN}"
INITSCRIPT_PARAMS_${PN} = "start 95 1 2 3 4 5 . stop 85 0 6 ."
INITSCRIPT_NAME_${PN}-keepalive = ""
INITSCRIPT_PARAMS_${PN}-keepalive = ""

do_install_append() {
	install -d ${D}${sysconfdir}/defaultconfig
	install -d ${D}${sysconfdir}/defaultconfig/config
	install -d ${D}${sysconfdir}/defaultconfig/config/watchdog.d
	install -m 0755 ${WORKDIR}/cksystemd.sh ${D}${sysconfdir}/defaultconfig/config/watchdog.d/cksystemd.sh
	install -m 0755 ${WORKDIR}/ckhwwdt.sh ${D}${sysconfdir}/defaultconfig/config/watchdog.d/ckhwwdt.sh

	rm -f ${D}${sysconfdir}/init.d/wd_keepalive
	#rm -f ${D}${sbindir}/wd_keepalive
}

FILES_${PN} += "${sysconfdir}/defaultconfig/config/* \
            "
FILES_${PN}-keepalive = " \
    ${sbindir}/wd_keepalive \
"

#!/bin/sh

# Include the table of exit codes.
readonly ENOERR=0             # /* no error */
readonly ESRCH=3              # /* No such process */
readonly ENODEV=19            # /* No such device */
readonly EINVAL=22            # /* Invalid argument */
readonly ERESET=254           # /* unconditional hard reset */

# Get the name of this script
bname=$(basename $0)
bnametest="$bname""_test"
bnamerepair="$bname""_repair"

readonly LOCKFILE_DIR=/var/lock
readonly LOCK_FD_TEST=991
readonly LOCK_FD_REPAIR=992
__PP=systemd
__PID=1
__PP_SH=systemd-shutdown
__PID_SH=1
__TRYMAX=6
__SAMPLE_PERIOD=5
WDTKILL_TIMEOUT=60
__SYSDPROCESS="systemd-journald.service"
__SYSCTLERR="Failed to get properties:"
__SYSCTLERR2="timed out"
__SYSCTLCMD="systemctl --no-pager status"
__SYSSAMPLE_PERIOD=10

# this file will log attempt to reset the system from this script
TRACEFILE=/mnt/storage/.watchdog_tst.log

lock() {
    local prefix=$1
    local llock_fd=$2
    local fd=${2:-$llock_fd}
    local lock_file=$LOCKFILE_DIR/$prefix.lock

    # create lock file
    eval "exec $fd>$lock_file"

    # acquier the lock
    flock -n $fd && return 0 || return 1
}

create_mytrace ()
{
	UPTIME=$(cat /proc/uptime | cut -d. -f1)
	DATE=$(date)
	echo "UPTIME=${UPTIME}" > $TRACEFILE
	echo "DATE=${DATE}" >> $TRACEFILE
	echo $1 >> $TRACEFILE
	sync
}

chk_systemctl ()
{
	local merr=$ENOERR

	SCTLCHK_RES=$(${__SYSCTLCMD} ${__SYSDPROCESS} 2>&1)
	SCTLCHK_RET=$?
	if [[ $SCTLCHK_RES == *"${__SYSCTLERR}"* ]] && [[ $SCTLCHK_RES == *"${__SYSCTLERR2}"* ]] && [[ ${SCTLCHK_RET} -ne 0 ]]; then merr=$ENODEV; fi
	return $merr
}

run_test ()
{
	local err=$ENOERR
	#echo "Running test"

    PDOF_RES=$(pidof ${__PP})
    PDOF_RET=$?

    __COUNTER=1
    if test ${PDOF_RET} -eq 0 && test ${PDOF_RES} = ${__PID}; then
	   sleep ${__SYSSAMPLE_PERIOD}
	   chk_systemctl
	   err=$?
    else
        PDOF_RES=$(pidof ${__PP_SH})
        PDOF_RET=$?
        if test ${PDOF_RET} -eq 0 && test ${PDOF_RES} = ${__PID_SH}; then
            err=$ENOERR
        else
            until test $__COUNTER -gt ${__TRYMAX}; do
                sleep ${__SAMPLE_PERIOD}
                PDOF_RES=$(pidof ${__PP})
                PDOF_RET=$?
                if test ${PDOF_RET} -eq 0 && test ${PDOF_RES} = ${__PID}; then
                   break;
                else
                    PDOF_RES=$(pidof ${__PP_SH})
                    PDOF_RET=$?
                    if test ${PDOF_RET} -eq 0 && test ${PDOF_RES} = ${__PID_SH}; then
                        break;
                    else
                        __COUNTER=$((__COUNTER+1))
                    fi
                fi
            done
            if test $__COUNTER -le ${__TRYMAX}; then
                err=$ENOERR
            else
                err=$ESRCH
            fi
        fi
    fi
    return $err
} # End of run_test


run_repair ()
{
	local param="$1"
	local object="$2"
	local err=$ENOERR

	if [ -z "$param" ] ; then
		echo "Missing error code (parameter) for repair"
		return $EINVAL
	fi

	if [ -z "$object" ] ; then
		echo "Missing script name for repair"
		return $EINVAL
	fi

	#echo "Running repair $param $object"

	case "$param" in
        $ESRCH) # No such process...
            PDOF_RES=$(pidof ${__PP})
            PDOF_RET=$?

            if test ${PDOF_RET} -eq 0 && test ${PDOF_RES} = ${__PID}; then
                err=$ENOERR
            else
                PDOF_RES=$(pidof ${__PP_SH})
                PDOF_RET=$?
                if test ${PDOF_RET} -eq 0 && test ${PDOF_RES} = ${__PID_SH}; then
                    err=$ENOERR
                else
                    #err=$ESRCH
                    # attempt to halt, in case not working kill watchdog daemon, and exit with hard reset code
                    echo "cksystemd: systemd is gone, attempting to halt and/or watchdog reset!"
                    err=$ERESET
                    ( sleep $(($WDTKILL_TIMEOUT)) ; killall -9 watchdog ) &
                    create_mytrace "cksystemd: systemd is gone, attempting to halt and/or watchdog reset!"
                    systemctl halt
                fi
            fi
            ;;

        $ENODEV) # No such device...
            chk_systemctl
            CHK_ERR=$?

            if test ${CHK_ERR} -eq 0 ; then
                err=$ENOERR
            else
                #err=$ENODEV
                # attempt to halt, in case not working kill watchdog daemon, and exit with hard reset code
                echo "cksystemd: systemctl is gone, attempting to halt and/or watchdog reset!"
                err=$ERESET
                ( sleep $(($WDTKILL_TIMEOUT)) ; killall -9 watchdog ) &
                create_mytrace "cksystemd: systemctl is gone, attempting to halt and/or watchdog reset!"
                systemctl halt
            fi
            ;;

        *)	# Unknown error codes for our attempt.
                err=$ENOERR
                #err=$EINVAL
                ;;
	esac

	return $err
} # End of run_repair

err=$ENOERR

	# Process the command line.
	case "$1" in
        test)
            # Lock test so a single instance of test is running in test mode
            lock $bnametest $LOCK_FD_TEST || exit "$ENOERR"

            run_test
            err=$? # Get function return value.
            ;;

        repair)
            # Lock test so a single instance of test is running in repair mode
            lock $bnamerepair $LOCK_FD_REPAIR || exit "$ENOERR"

            #echo "PARAM:$0 $1 $2 $3"
            run_repair "$2" "$0"
            err=$? # Get function return value.
            #err=0
            ;;

        *)
            echo "Usage: $bname {test|repair errcode scriptname}"
            err=$EINVAL
        esac

#echo "Exiting with value=$err"
exit $err

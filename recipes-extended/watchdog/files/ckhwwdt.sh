#!/bin/sh
# Checks if hw wdt is enabled and protects the system

# Include the table of exit codes.
readonly ENOERR=0             # /* no error */
readonly EIO=5                # /* I/O error */
readonly EINVAL=22            # /* Invalid argument */
readonly ERESET=254           # /* unconditional hard reset */
readonly EREBOOT=255          # /* unconditionnal reboot */

# Get the name of this script
bname=$(basename $0)
bnametest="$bname""_test"
bnamerepair="$bname""_repair"

readonly LOCKFILE_DIR=/var/lock
readonly LOCK_FD_TEST=993
readonly LOCK_FD_REPAIR=994
__TRYMAX=2
__SAMPLE_PERIOD=90
REBOOT_TIMEOUT=120
PENDINGCHK=/tmp/.boot_pending
HWSTATE=/var/run/wdtstate

# this file will log attempt to reset the system from this script
TRACEFILE=/mnt/storage/.watchdog_tst1.log

lock() {
	local prefix=$1
	local llock_fd=$2
	local fd=${2:-$llock_fd}
	local lock_file=$LOCKFILE_DIR/$prefix.lock

	# create lock file
	eval "exec $fd>$lock_file"

	# acquier the lock
	flock -n $fd && return 0 || return 1
}

create_mytrace ()
{
	UPTIME=$(cat /proc/uptime | cut -d. -f1)
	DATE=$(date)
	echo "UPTIME=${UPTIME}" > $TRACEFILE
	echo "DATE=${DATE}" >> $TRACEFILE
	echo $1 >> $TRACEFILE
	sync
}

run_test ()
{
	local err=$ENOERR
	#echo "Running test"
	if [ -f "$HWSTATE" ]; then
		read -r FWDTSTATE < $HWSTATE
	else
		FWDTSTATE=""
	fi
	__COUNTER=1
	until test $__COUNTER -gt ${__TRYMAX}; do
		# Because the cpu burden of i2c transactions checking the watchdog, makes sure waiting to sample not so often...
		sleep ${__SAMPLE_PERIOD}
		if [ -f "$FWDTSTATE" ] ; then
			wdtstate=$(cat $FWDTSTATE)
		else
			wdtstate=0
		fi
		if test ${wdtstate} -eq 1; then
			break;
		else
			__COUNTER=$((__COUNTER+1))
		fi
	done
	if test $__COUNTER -le ${__TRYMAX}; then
		err=$ENOERR
	else
		err=$EIO
	fi
	return $err
} # End of run_test


run_repair ()
{
	local param="$1"
	local object="$2"
	local err=$ENOERR

	if [ -z "$param" ] ; then
		echo "Missing error code (parameter) for repair"
		return $EINVAL
	fi

	if [ -z "$object" ] ; then
		echo "Missing script name for repair"
		return $EINVAL
	fi

	#echo "Running repair $param $object"

	case "$param" in
	$EIO) # I/O error...
		if [ -f "$HWSTATE" ]; then
			read -r FWDTSTATE < $HWSTATE
		else
			FWDTSTATE=""
		fi
		if [ -f "$FWDTSTATE" ] ; then
			wdtstate=$(cat $FWDTSTATE)
		else
			wdtstate=0
		fi

		if test ${wdtstate} -eq 1; then
			err=$ENOERR
		else
			#err=$EIO
			# attempt to reboot, and exit with reboot code
			echo "ckhwwdt: no hw wdt protection, attempting to reboot!"
			err=$EREBOOT
			touch ${PENDINGCHK}
			sleep $(($REBOOT_TIMEOUT))
			create_mytrace "ckhwwdt: no hw wdt protection, attempting to reboot!"
			systemctl reboot
		fi
	;;

	*)	# Unknown error codes for our attempt.
		err=$ENOERR
		#err=$EINVAL
	;;
	esac

	return $err
} # End of run_repair

	err=$ENOERR

	# Process the command line.
	case "$1" in
	test)
		# Lock test so a single instance of test is running in test mode
		lock $bnametest $LOCK_FD_TEST || exit "$ENOERR"

		run_test
		err=$? # Get function return value.
	;;

	repair)
		# Lock test so a single instance of test is running in repair mode
		lock $bnamerepair $LOCK_FD_REPAIR || exit "$ENOERR"

		#echo "PARAM:$0 $1 $2 $3"
		run_repair "$2" "$0"
		err=$? # Get function return value.
		#err=0
	;;

	*)
		echo "Usage: $bname {test|repair errcode scriptname}"
		err=$EINVAL
	esac

#echo "Exiting with value=$err"
exit $err

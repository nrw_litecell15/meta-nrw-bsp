#!/bin/sh
# Define this variable _NOSPINDLE before calling this script if this script run through a systemd service

TAG=makefl:
REFFIL="./.flst"
dir_list_file=""
list_file=""
ENDSIG=/tmp/makefl_sig
ERRSIG=/tmp/makefl_err
PENDINGCHK=/tmp/.boot_pending
MYFS=""
tempdir=""
FORCEOVERWR=0

# lock wait time max 15mn * 60 = 900 secs (sufficient time to update)
LOCKWAIT=900
# WARNING: lock must match syncfs/update/forcerecover one
bname=updateperm

sp="/-\|"
sc=0
spin() {
    if [ -z "$_NOSPINDLE" ]; then
        printf "\r${TAG} ${sp:sc++:1}"
        ((sc==${#sp})) && sc=0
    else
        printf "${TAG} ${sp:sc++:1} \n"
        ((sc==${#sp})) && sc=0
    fi
}
endspin() {
    if [ -z "$_NOSPINDLE" ]; then
        printf "\r"
    fi
}

readonly LOCKFILE_DIR=/var/lock
readonly LOCK_FD=997

lock() {
    local prefix=$1
    local fd=${2:-$LOCK_FD}
    local lock_file=$LOCKFILE_DIR/$prefix.lock

    # still allow to run unlocked if the lock dir does not exist
    if [ ! -d "${LOCKFILE_DIR}" ]; then
	return 0
    fi

    # create lock file
    eval "exec $fd>$lock_file"
    # still allow to run unlocked if the lock file cannot be created
    if [ $? -ne 0 ]; then
	return 0
    fi

    # acquier the lock
    flock -w ${LOCKWAIT} $fd \
        && return 0 \
        || return 1
}

unlock() {
    local prefix=$1
    local fd=${2:-$LOCK_FD}
    local lock_file=$LOCKFILE_DIR/$prefix.lock

    # release the lock
    flock -u $fd
    sync
}

function log_write()
{
	echo "$*"
}

function log_write_nr()
{
	echo -n "$*"
}

function my_exit()
{
	trap - SIGINT
	trap - SIGQUIT
	trap - SIGTERM
	trap - SIGHUP
    sync
    if [ -f "${ENDSIG}" ]; then
        rm -f "${ENDSIG}"
    fi
    if [ -f "${dir_list_file}" ]; then
        rm -f "${dir_list_file}"
    fi
    if [ -f "${list_file}" ]; then
        rm -f "${list_file}"
    fi
	cd "$_curr_dir"
    if [ -d "${tempdir}" ]; then
        if mount | grep -q "${tempdir}"; then
            umount "${tempdir}"
        fi
        rmdir "$tempdir"
    fi
	unlock $bname
	exit $*
}

function __sig_int {
    log_write " "
    log_write "$TAG WARNING: SIGINT caught"
    my_exit 110
}

function __sig_quit {
    log_write " "
    log_write "$TAG WARNING: SIGQUIT caught"
    my_exit 111
}

function __sig_term {
    log_write " "
    log_write "$TAG WARNING: SIGTERM caught"
    my_exit 112
}

function __sig_hup {
    log_write " "
    log_write "$TAG WARNING: SIGHUP caught"
    my_exit 113
}

function show_help {
    log_write "makefl help information:"
    log_write "makefl [[-f] | [--help]] <fspath>"
    log_write "create the reference analysis file for specified file system path."
    log_write "Warning: makes sure file system could not be modified while doing this."
    log_write "where <fspath>           # path where file system to analyze is mounted"
    log_write "      options: -f        # force to overwrite file system reference file if already exist"
    log_write "                         # CAUTION: use this option if you know what you are doing"
    log_write "               --help    # displays this help"
    log_write "example: makefl /tmp/mounted_fs; # when fs is mounted there"
}

# return 0 if no error, anything if any problem
# 1st parameter is the file list file, 2nd is the resulting output file
function mychkfiles {
    while read LINE; do
	if [ $LINE != ${REFFIL} ] && [ $LINE != "./lost+found" ]; then
	    if [ -d $LINE ] && [ ! -h $LINE ] && [ ! -L $LINE ]; then
                MYDIR="0"
            else
                MYDIR="%s"
            fi
            LC_ALL=C stat --printf "%N,$MYDIR,%F,%a,%u,%g\n" $LINE >> $2
            if [ $? -ne 0 ]; then
                log_write "$TAG stat file ($LINE) fail!"
                return 50
            fi
            if [ -f $LINE ] && [ ! -h $LINE ] && [ ! -L $LINE ]; then
                md5sum $LINE >> $2
                if [ $? -ne 0 ]; then
                    log_write "$TAG md5sum file ($LINE) fail!"
                    return 51
                fi
            fi
        fi
    done < $1;
    sed -i -e 's/'\''\$'\''//g' -e 's/'\'''\''//g' $2
    if [ $? -ne 0 ]; then
        log_write "$TAG cannot filter result file!"
        return 52
    fi
}

if [ -e "${PENDINGCHK}" ]; then
    log_write "$TAG Pending reboot, could not run!"
    exit 97
fi

_curr_dir=`pwd`

# Lock to test a single instance of makefl is running, and exit if wait timeout
log_write "$TAG Checking if allowed to run..."
lock $bname || ( log_write "$TAG Checking if allowed to run... failed"; exit 100 )
log_write "$TAG Checking if allowed to run... done"

# Set TRAPs to release lock if forced to exit
trap __sig_int SIGINT
trap __sig_quit SIGQUIT
trap __sig_term SIGTERM
trap __sig_hup SIGHUP

if [ -e "${PENDINGCHK}" ]; then
    log_write "$TAG Pending reboot, could not run!"
    my_exit 97
fi

TOTALARG=$#
while getopts :f- FLAG; do
    case $FLAG in
        f)
        #log_write "#Force overwriting analysis file option specified (-f)"
        FORCEOVERWR=1;;
        '-')
        show_help
        my_exit 0;;
        \?)
        log_write "Invalid option: -$OPTARG" && my_exit 1;;
        \:)
        log_write "Required argument not found for option: -$OPTARG" && my_exit 2;;
  esac
done

# removes processed option(s) from the cmd line args
shift $((OPTIND-1))

if [ "$#" -ne 1 ]; then
    show_help
    my_exit 3
fi

MYFS=$(realpath $1)
if [ $? -ne 0 ]; then
    log_write "$TAG Cannot get real path of ($1)!"
    my_exit 4
fi
tempdir=`mktemp -d`
if [ $? -ne 0 ]; then
    log_write "$TAG could not create tmp directory!"
    my_exit 5
fi
mount_res=$(mount --bind -o ro ${MYFS} ${tempdir} 2>&1)
mount_ret=$?
if [ ${mount_ret} -ne 0 ]; then
    log_write "$TAG mount ${MYFS} fail!"
    my_exit 6
fi
log_write "$TAG Analyzing (${MYFS})"
cd ${tempdir}
if [ $? -ne 0 ]; then
    log_write "$TAG Change directory (${tempdir}) fail!"
    my_exit 7
fi
# makes sure that fs has not a valid reference file if not force to overwrite it
if [ ${FORCEOVERWR} -eq 0 ]; then
    if [ -f ${REFFIL} ]; then
        log_write "$TAG Warning, analysis reference file already exist!"
        my_exit 8
    fi
fi
dir_list_file=`mktemp`
if [ $? -ne 0 ]; then
    log_write "$TAG could not create tmp list file!"
    my_exit 9
fi
list_file=`mktemp`
if [ $? -ne 0 ]; then
    log_write "$TAG could not create tmp analysis file!"
    my_exit 10
fi
rm -f ${ENDSIG}
rm -f ${ERRSIG}
log_write "$TAG Building files list..."
(
find . | LC_ALL=C sort > ${dir_list_file}
if [ $? -ne 0 ]; then
    log_write "$TAG find files fail!"
    touch ${ERRSIG}
    exit 11
fi
touch ${ENDSIG}
) &
until [ -f ${ENDSIG} ]; do
    spin
    if [ -f ${ERRSIG} ]; then
        my_exit 12
    fi
    if [ ! -f ${ENDSIG} ]; then
        sleep 1s
    fi
done
endspin
log_write "$TAG Building files list... done"
rm -f ${ENDSIG}
rm -f ${ERRSIG}
log_write "$TAG Analyzing files..."
(
mychkfiles ${dir_list_file} ${list_file}
if [ $? -ne 0 ]; then
    log_write "$TAG Analyzing fail!"
    touch ${ERRSIG}
    exit 13
fi
touch ${ENDSIG}
) &
until [ -f ${ENDSIG} ]; do
    spin
    if [ -f ${ERRSIG} ]; then
        my_exit 14
    fi
    if [ ! -f ${ENDSIG} ]; then
        sleep 1s
    fi
done
endspin
log_write "$TAG Analyzing files... done"
log_write "$TAG Saving reference list..."
rem_res=$(mount -o remount,rw ${tempdir} 2>&1)
rem_ret=$?
if [ ${rem_ret} -ne 0 ]; then
    log_write "$TAG cannot remount ${tempdir} to rw!"
    my_exit 15
fi
cp -f ${list_file} ${REFFIL}
if [ $? -ne 0 ]; then
    log_write "$TAG could not create reference list file!"
    my_exit 16
fi
log_write "$TAG Saving reference list... done"
my_exit 0

#!/bin/sh
cp -f /usr/bin/.update /tmp/.update
if [ $? -ne 0 ]; then
    echo "Cannot create file /tmp/.update, code: $?"
    exit 98
fi
# Starts a lower priority version of update replacing current shell
exec nice -n 5 /tmp/.update "$@"


FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "nginx_%.bbappend"
PR       := "${PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

SRC_URI += "file://htpasswd \
	file://.ndpasswd \
	   "

# our current configuration depend on the presence of these runtime packages
RDEPENDS_${PN} += "netdata webmin"

do_install_append() {
	if ${@bb.utils.contains('DISTRO_FEATURES','systemd','true','false',d)};then
		install -d 0755 ${D}${sysconfdir}/systemd/system/multi-user.target.wants
		ln -sf ${base_libdir}/systemd/system/nginx.service ${D}${sysconfdir}/systemd/system/multi-user.target.wants/nginx.service
	fi
	install -d ${D}${sysconfdir}/defaultconfig
	install -d ${D}${sysconfdir}/defaultconfig/config
	install -d ${D}${sysconfdir}/defaultconfig/config/nginx
	install -m 0755 ${WORKDIR}/htpasswd ${D}${sbindir}/htpasswd
	install -m 0644 ${WORKDIR}/.ndpasswd ${D}${sysconfdir}/defaultconfig/config/nginx/.ndpasswd
	ln -sf /mnt/rom/user/config/nginx/.ndpasswd ${D}${sysconfdir}/nginx/.ndpasswd

}

INITSCRIPT_PARAMS = "defaults"

FILES_${PN} += "\
	${sbindir}/htpasswd \
	${sysconfdir}/defaultconfig/config/nginx/.ndpasswd \
	${sysconfdir}/nginx/.ndpasswd \
	"

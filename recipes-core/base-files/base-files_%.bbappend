FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "base-files_%.bbappend"
PR       := "${PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/${PN}"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

SRC_URI += "file://alignment.service \
            file://environment \
            file://ssh_login.sh \
            file://bash_history.sh \
            file://hostname.service \
            file://mountuser.service \
            file://mountuser \
            file://bash_aliases.sh \
            file://systemdcust.service \
            file://systemdcust \
            file://setled.service \
            "

dirs755 = "/bin /boot /dev ${sysconfdir} ${sysconfdir}/default \
           ${sysconfdir}/skel /lib /mnt /proc ${ROOT_HOME} /run /sbin \
           ${prefix} ${bindir} ${docdir} /usr/games ${includedir} \
           ${libdir} ${sbindir} ${datadir} ${localstatedir}/lib \
           ${datadir}/common-licenses ${datadir}/dict ${infodir} \
           ${mandir} ${datadir}/misc ${localstatedir} \
           ${localstatedir}/backups \
           /sys ${localstatedir}/lib/misc ${localstatedir}/spool \
           ${localstatedir}/volatile \
           ${localstatedir}/volatile/log \
           /home ${prefix}/src ${localstatedir}/local \
           /media /mnt/storage /mnt/tmp \
           /mnt/rom /mnt/rom/factory /mnt/rom/user ${sysconfdir}/systemd /srv \
           ${sysconfdir}/systemd/system/multi-user.target.wants \
           ${base_libdir}/systemd/system \
           ${sysconfdir}/profile.d \
           ${sysconfdir}/defaultconfig \
           ${sysconfdir}/defaultconfig/config \
           "


do_install_append() {
	# Add alignment service
	install -m 0644 ${S}/alignment.service ${D}${base_libdir}/systemd/system/alignment.service
	ln -sf ${base_libdir}/systemd/system/alignment.service  ${D}${sysconfdir}/systemd/system/multi-user.target.wants/alignment.service
	# Setup systemd for no colors
	install -m 0644 ${S}/environment ${D}${sysconfdir}/environment
	install -m 0644 ${S}/ssh_login.sh ${D}${sysconfdir}/profile.d/ssh_login.sh
	install -m 0644 ${S}/bash_history.sh ${D}${sysconfdir}/profile.d/bash_history.sh
	install -m 0644 ${S}/bash_aliases.sh ${D}${sysconfdir}/profile.d/bash_aliases.sh
	# Setup for runtime config hostname
	install -m 0644 ${D}${sysconfdir}/hostname ${D}${sysconfdir}/defaultconfig/config/hostname
	rm -rf ${D}${sysconfdir}/hostname
	ln -sf /mnt/rom/user/config/hostname ${D}${sysconfdir}/hostname
	# Add hostname service
	install -m 0644 ${S}/hostname.service ${D}${base_libdir}/systemd/system/hostname.service
	ln -sf ${base_libdir}/systemd/system/hostname.service  ${D}${sysconfdir}/systemd/system/multi-user.target.wants/hostname.service
	# Add mountuser service
	install -m 0644 ${S}/mountuser.service ${D}${base_libdir}/systemd/system/mountuser.service
	ln -sf ${base_libdir}/systemd/system/mountuser.service  ${D}${sysconfdir}/systemd/system/multi-user.target.wants/mountuser.service
	install -m 0755 ${S}/mountuser ${D}${bindir}/mountuser
	# Add user customization service
	install -m 0644 ${S}/systemdcust.service ${D}${base_libdir}/systemd/system/systemdcust.service
	ln -sf ${base_libdir}/systemd/system/systemdcust.service ${D}${sysconfdir}/systemd/system/multi-user.target.wants/systemdcust.service
	install -m 0755 ${S}/systemdcust ${D}${bindir}/systemdcust
	# Setup for runtime motd
	install -m 0644 ${D}${sysconfdir}/motd ${D}${sysconfdir}/defaultconfig/config/motd
	rm -rf ${D}${sysconfdir}/motd
	ln -sf /mnt/rom/user/config/motd ${D}${sysconfdir}/motd
	# Makes sure new users will have correct prompt through their home directory
	sed -i '/export PS1=/d' ${D}${sysconfdir}/skel/.bashrc
	echo "export PS1='\u@\h:\w\$ '" >> ${D}${sysconfdir}/skel/.bashrc
	# Add setled service
	install -m 0644 ${S}/setled.service ${D}${base_libdir}/systemd/system/setled.service
	ln -sf ${base_libdir}/systemd/system/setled.service  ${D}${sysconfdir}/systemd/system/multi-user.target.wants/setled.service
}

FILES_${PN} += "${bindir}/mountuser \
            ${bindir}/systemdcust \
            ${sysconfdir}/systemd/system \
            ${base_libdir}/systemd/system \
            "

BASEFILESISSUEINSTALL = "do_custom_baseissueinstall"

do_custom_baseissueinstall() {
        do_install_basefilesissue
        install -m 644 ${WORKDIR}/issue*  ${D}${sysconfdir}
        printf "LiteCell 1.5 BSP image (${DISTRO_VERSION}) " >> ${D}${sysconfdir}/issue
        printf "LiteCell 1.5 BSP image (${DISTRO_VERSION}) " >> ${D}${sysconfdir}/issue.net
        printf "\\\n \\\l\n" >> ${D}${sysconfdir}/issue
        echo "%h" >> ${D}${sysconfdir}/issue.net
        echo >> ${D}${sysconfdir}/issue
        echo >> ${D}${sysconfdir}/issue.net
}


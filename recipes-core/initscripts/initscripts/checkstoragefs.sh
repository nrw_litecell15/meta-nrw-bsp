#!/bin/sh

STORAGE_DEVICE=/dev/mmcblk0p4
STORAGE=/mnt/storage
PENDINGCHK=/tmp/.boot_pending
MYACC=777

function checkstorageacc {
    if [ "x$MYACC" == "x$(stat -c %a $1)" ]; then
        return 0
    fi
    chmod $MYACC $1
    if [ $? -ne 0 ]; then
        echo "Cannot set mode of Storage device."
    fi
    sync
}

# makes sure we don't attempt any storage repair if possible rootfs repair(reboot) indicated by PENDINGCHK is pending to avoid further corruption
if [ ! -e "${PENDINGCHK}" ]; then
    umount $STORAGE_DEVICE > /dev/null
    e2fsck $STORAGE_DEVICE -p -f > /dev/null

    if [ $? -gt 1 ]; then
        echo "Storage device is corrupted."
        mkfs -t ext4 -F $STORAGE_DEVICE
        if [ $? -ne 0 ]; then
            echo "Cannot create Storage device."
        else
            echo "Storage device restored."
        fi
    else
        echo "Storage device is healthy."
    fi

    mount $STORAGE_DEVICE > /dev/null
    if [ $? -eq 0 ]; then
        checkstorageacc $STORAGE
    else
        echo "Cannot mount Storage device."
    fi
else
    echo "Pending reboot, could not run!"
fi

exit 0

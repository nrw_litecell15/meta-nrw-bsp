#!/bin/sh
### BEGIN INIT INFO
# Provides:          checkroot
# Required-Start:    udev
# Required-Stop:     
# Default-Start:     S
# Default-Stop:
# Short-Description: Check to root file system.
### END INIT INFO

# mmcblk0:
# p1: FAT (dev only)
# p2: EXT4 (master)
# p3: EXT4 (alt)
# p4: EXT4 (storage)

export _NOSPINDLE=1
WDTKILL_TIMEOUT=3
MONOLITHIC_UPDATE_TIMEOUT=15
MASTER=/dev/mmcblk0p2
ALT=/dev/mmcblk0p3
PENDING=/mnt/storage/.updatealt_pending
PENDINGABORT=/tmp/.updatealt_abort
PENDING_TIMEOUT=60
PENDINGCHK=/tmp/.boot_pending
ONECHKBOOTOVER=/tmp/.checkboot_over
ZEROFILE=/tmp/.zerofile_chkroot

STAG=checkroot:

rootfspart() {
    if mount | grep "on / type ext4" | grep -q mmcblk0p2; then
        echo "master"
    elif mount | grep "on / type ext4" | grep -q mmcblk0p3; then
        echo "alt"
    else
        echo "nfs"
    fi
}

ckfs() {
    echo "Checking filesystem..."
    e2fsck -f -p $1
    myret=$?
    echo "Checking filesystem... done"
    return ${myret}
}

rebootalt() {
    # makes sure all bootable file are ok if not repair them
    touch ${ONECHKBOOTOVER}
    /usr/bin/checkboot
    # Makes sure we won't attempt any update that could be interrupted by following reboot
    touch ${PENDINGCHK}
    FILE=$(find /sys -name bootcount)
    FWDTSTATE=$(find /sys -name wdt_hw_state)
    wdtstate=0
    if [ ! -f "$FWDTSTATE" ] ; then
	FWDTSTATE="$ZEROFILE"
	echo "0" > ${FWDTSTATE}
    fi
    # Wait and kill watchdog daemon to reboot on alt
    echo "Need to schedule reboot on alt (waiting for watchdog process 1st)"
    (
        WDTEST_RET=1
        until [ $WDTEST_RET -eq 0 ]; do
		WDTEST=$(ps aux | grep "watchdog" | grep "/usr/sbin/")
		WDTEST_RET=$?
		if test ${WDTEST_RET} -ne 0; then
			sleep 15s
		fi
        done
        echo "Scheduling reboot on alt"
	sleep $(($WDTKILL_TIMEOUT * 60)) && echo 254 > ${FILE} && killall -9 watchdog && wdtstate=$(cat $FWDTSTATE) && if [ $wdtstate -eq 1 ]; then halt; else echo "Error wdt is not enabled, alt reboot aborted, rebooting..." ; sleep 5s; reboot; fi
    ) &
}

safereboot() {
    # makes sure all bootable file are ok, if not repair them
    touch ${ONECHKBOOTOVER}
    /usr/bin/checkboot
    reboot
}

isclean() {
    TMP=$(mktemp -d)
    RET=1
    if mount -o ro $1 $TMP; then
        if [ -e "${TMP}/clean" ]; then
            # partition is clean
            RET=0
        fi
    fi
    umount $TMP
    rmdir $TMP
    sync
    return $RET
}

safesyncfs() {
    export _SAFESYNC=1
    syncfs $1
    RET=$?
    unset _SAFESYNC
    return $RET
}

nsyncfs() {
    unset _SAFESYNC
    syncfs $1
    RET=$?
    return $RET
}

ROOTFSPART=$(rootfspart)

# makes sure this file does not exist (normally not) to prevent any interference in other check... scripts
rm -f ${PENDINGCHK}
rm -f ${ONECHKBOOTOVER}

# Ensure that root is quiescent and read-only before fsck'ing
mount -n -o remount,ro /

if [ "$ROOTFSPART" == "nfs" ]; then
    # Recovery boot sequence
    echo ""
    echo "Recovery boot from nfs"
    echo ""
    ckfs ${MASTER}
    CHKMASTER=$?
    ckfs ${ALT}
    CHKALT=$?
    MASTERFSOK=yes
    ALTFSOK=yes

    if [ "$CHKMASTER" -gt 1 ]; then
        echo ""
        echo "Master partition is corrupted."
        echo ""
        MASTERFSOK=no
    else
        if ! isclean ${MASTER}; then
            echo "Master partition is not clean."
            MASTERFSOK=no
        fi
    fi

    if [ "$CHKALT" -gt 1 ]; then
        echo ""
        echo "Alt partition is corrupted."
        echo ""
        ALTFSOK=no
    else
        if ! isclean ${ALT}; then
            echo "Alt partition is not clean."
            ALTFSOK=no
        fi
    fi

    if [ "$MASTERFSOK" == "no" ]; then
        if [ -f /clean ]; then
            echo ""
            #echo "WARN: errors detected on master, syncing master from nfs rootfs."
            #echo ""
            #safesyncfs ${MASTER}
            # we cannot repair both fs at the same time because of WDT, so schedule a reboot to repair later if needed
            if ([ "$?" -eq 0 ] && [ "$ALTFSOK" == "no" ]); then
                echo ""
                #echo "WARN: errors detected on alt, scheduling a reboot for later repair..."
                #echo ""
                #touch ${PENDINGCHK}
                #( sleep $(($WDTKILL_TIMEOUT * 60)) && safereboot ) &
            fi
            #rm -f ${PENDING} ; sync
        else
            echo ""
            #echo "WARN: nfs rootfs not clean, could not be used to repair master."
            #echo ""
        fi
    else
        if [ "$ALTFSOK" == "no" ]; then
            if [ -f /clean ]; then
                echo ""
                #echo "WARN: errors detected on alt, syncing alt from nfs rootfs."
                #echo ""
                #safesyncfs ${ALT} && rm -f ${PENDING} ; sync
            else
                echo ""
                #echo "WARN: nfs rootfs not clean, could not be used to repair alt."
                #echo ""
            fi
        else
            echo ""
            echo "Master and alt are clean"
            echo ""
        fi
    fi
elif [ "$ROOTFSPART" == "master" ]; then
    # Normal boot sequence
    ckfs ${MASTER}

    if [ "$?" -gt 1 ]; then
        echo ""
        echo "Master partition is corrupted."
        echo ""

        ckfs ${ALT}

        if [ "$?" -gt 1 ]; then
            echo ""
            echo "WARN: both filesystems are corrupted beyond repair,"
            echo "      continue booting and hope for the best."
            echo ""
        else
            rebootalt
        fi
    else
        # Check alt partition
        FSOK=yes

        # Note: Do not verify clean state if filesystem is corrupted.
        
        ckfs ${ALT}
        if [ "$?" -gt 1 ]; then
            FSOK=no
        elif ! isclean ${ALT}; then
            FSOK=no
        fi

        if [ -f /clean ]; then
            if [ "$FSOK" == "no" ]; then
                echo ""
                echo "WARN: errors detected on alt, syncing."
                echo ""
                safesyncfs ${ALT} && rm -f ${PENDING} ; sync
            else
                echo ""
                echo "Master and alt are clean"
                echo ""
                if [ -f ${PENDING} ]; then
                    echo ""
                    echo "Schedule sync for updated master."
                    echo ""
                    ( sleep $(($PENDING_TIMEOUT * 60)) && if [ ! -f ${PENDINGABORT} ] && [ -f ${PENDING} ]; then nsyncfs ${ALT} && rm -f ${PENDING} ; sync; fi ) &
                fi
            fi
        else
            if [ "$FSOK" == "no" ]; then
                echo ""
                echo "WARN: both filesystems contain errors. CANNOT REPAIR."
                echo ""
            else
                echo ""
                echo "WARN: Master not clean, rebooting on alt"
                echo ""
                rebootalt
            fi
        fi
    fi
else
    # Alt boot sequence
    ckfs ${ALT}

    if [ "$?" -gt 1 ]; then
        echo ""
        echo "Alt partition is corrupted"
        echo ""

        ckfs ${MASTER}

        if [ "$?" -gt 1 ]; then
            echo ""
            echo "WARN: both filesystems are corrupted beyond repair,"
            echo "      continue booting and hope for the best."
            echo ""
        else
            echo ""
            echo "Rebooting on master"
            echo ""
            safereboot
        fi
    else
        # Check master partition
        FSOK=yes
        
        # Note: Do not verify clean state if filesystem is corrupted.

        ckfs ${MASTER}
        if [ "$?" -gt 1 ]; then
            FSOK=no
        elif ! isclean ${MASTER}; then
            FSOK=no
        fi

        if [ -f /clean ]; then
            if [ "$FSOK" == "no" ]; then
                echo ""
                echo "WARN: errors detected on master, syncing."
                echo ""
                safesyncfs ${MASTER}
                if [ -f ${PENDING} ]; then
                    rm -f ${PENDING}
                    sync
		fi
            else
                echo ""
                echo "Master and alt are clean"
                echo ""
                if [ -f ${PENDING} ]; then
                    echo ""
                    echo "Scheduled sync for updated master failed. Revert last update."
                    echo ""
                    safesyncfs ${MASTER} ; rm -f ${PENDING} ; sync
                else
                    FILE=$(find /sys -name bootcount)
                    bootcount=$(cat $FILE)
                    # If bootcount == 255 (254+1), an update triggered the boot on alt and sync must not occur.
                    # In other case, since booting on alt may indicate a failure in master, master must be synced.
                    if [ ! $bootcount -eq 255 ]; then
                        echo ""
                        echo "WARN: boot on alt triggered by watchdog, sync master and reboot on master."
                        echo ""
                        safesyncfs ${MASTER}
                    else
                        echo ""
                        echo "Boot on alt triggered by update."
                        echo ""
                        ( sleep $(($MONOLITHIC_UPDATE_TIMEOUT * 60)) && safereboot ) &
                        exit 0
                    fi
                fi
            fi
            touch ${PENDINGCHK}
            ( sleep $(($WDTKILL_TIMEOUT * 60)) && safereboot ) &
        else
            if [ "$FSOK" == "no" ]; then
                echo ""
                echo "WARN: both filesystems contain errors. CANNOT REPAIR."
                echo ""
            else
                echo ""
                echo "WARN: Alt not clean, rebooting on master"
                echo ""
                touch ${PENDINGCHK}
                ( sleep $(($WDTKILL_TIMEOUT * 60)) && safereboot ) &
            fi
        fi
    fi
fi

# Leave the filesystem ro

exit 0


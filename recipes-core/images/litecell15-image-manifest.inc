LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58"

install_lc15_manifest() {
    printf "    Build Date : %s\n" "`date -R`" > ${IMAGE_ROOTFS}/etc/litecell15.manifest
    printf "          UUID : %s\n" "`uuidgen -r`" >> ${IMAGE_ROOTFS}/etc/litecell15.manifest
    printf "       Machine : %s\n" "${MACHINE}" >> ${IMAGE_ROOTFS}/etc/litecell15.manifest
    printf "         Image : %s\n" "${IMAGE_NAME}" >> ${IMAGE_ROOTFS}/etc/litecell15.manifest
    printf "        Distro : %s\n" "${DISTRO}" >> ${IMAGE_ROOTFS}/etc/litecell15.manifest
    printf "Distro Version : %s\n" "${DISTRO_VERSION}" >> ${IMAGE_ROOTFS}/etc/litecell15.manifest
}

IMAGE_PREPROCESS_COMMAND += "install_lc15_manifest; "

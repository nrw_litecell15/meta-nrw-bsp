COMPATIBLE_MACHINE = "(?!omap-a15)"

# The size of the uncompressed ramdisk is 32MB
ROOTFS_SIZE = "32768"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = ""
PR       := "r${REPOGITFN}"
PV       := "${REPOGITT}-${REPOGITN}"

IMAGE_NAME = "${PN}-${PV}-${PR}-${DATETIME}"
BSP_LAYER_VERSION := "${PV}-${PR}"
IMAGE_INSTALL += "" 

export IMAGE_BASENAME = "litecell15-image"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58"

IMAGE_FEATURES += "package-management read-only-rootfs"

# 4KB per 1 inode should be enough
#dhrystone
EXTRA_IMAGECMD_ext2.gz += "-i 4096"

IMAGE_INSTALL += "\
    kernel-modules \
    packagegroup-core-boot \
    packagegroup-nrw-extra \
    kernel-devicetree \
    "

IMAGE_LINGUAS = ""

require ${PN}-manifest.inc
inherit core-image

install_lc15_manifest_append() {
    printf "           BSP : %s\n" "${BSP_LAYER_VERSION}" >> ${IMAGE_ROOTFS}/etc/litecell15.manifest
}

require ${PN}-user.inc


FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "busybox_%.bbappend"
PR       := "${PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"


SRC_URI_append = "\
	file://ftpget.cfg \
	file://ftpput.cfg \
	file://syslog.conf \
	file://busybox-syslog \
	file://busybox-syslog.service.in \
"

do_install_append() {
	if grep -q "CONFIG_SYSLOGD=y" ${B}/.config; then
		install -d 0644 ${D}${sysconfdir}/default
		install -d 0644 ${D}${sysconfdir}/defaultconfig
		install -d 0644 ${D}${sysconfdir}/defaultconfig/config

		#install syslog configuration to default configuration folder
		install -m 0644 ${WORKDIR}/busybox-syslog ${D}${sysconfdir}/defaultconfig/config/

		#use syslog OPTIONS file stored in user partition
		rm -f ${D}${sysconfdir}/default/busybox-syslog
		ln -sf /mnt/rom/user/config/busybox-syslog ${D}${sysconfdir}/default/busybox-syslog
	fi
}

FILES_${PN}-syslog += " \
		${sysconfdir}/default/* \
		${sysconfdir}/defaultconfig/config/* \
		"

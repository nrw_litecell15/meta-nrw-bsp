inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "netdata_%.bbappend"
PR       := "${PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/${PN}"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

# 17280 measures every 5 seconds => 1 day history
# Note that increasing the rate increase the CPU usage
# and increasing the history increase the RAM usage.
history = "17280"
update_every = "5"
hostname = "litecell15"
# default cachedir is "/var/cache" for ram mode, or "/mnt/storage/var/cache" for save mode
cachedir = "/var/cache"
# default run mode is everything in ram, nothing is saved in cachedir (possible modes: ram, save)
runmode = "ram"
# settings example for permanent cache mode (when runmode = "save" and cachedir is "/mnt/storage/var/cache")
#srunmodecache = "ExecStartPre=/bin/mkdir -p /mnt/storage/var/cache/netdata"
#srunmodeowncache = "ExecStartPre=/bin/chown -R nobody.netdata /mnt/storage/var/cache/netdata"
# settings example for ram mode (when runmode = "ram")
srunmodecache = "#"
srunmodeowncache = "#"

do_install_append() {
	chown -R nobody.netdata ${D}${datadir_native}/netdata/web
}

SUMMARY = "Busy loop test tool"
LICENSE = "CLOSED"

NRW_INTERNAL_MIRROR ??= ""

S = "${WORKDIR}/git"

RDEPENDS_${PN} = "glibc"

inherit gitver-pkg gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "busyloop_git.bb"
PR       := "r${REPOGITFN}"

PV   = "git${SRCPV}"
PKGV = "${PKGGITV}"

DEV_SRCREV  = "${AUTOREV}"
REL_SRCREV  = "6484f55c29cf9dc0f13008ed359955e9d90cead1"
SRCREV      = "${@ '${DEV_SRCREV}' if d.getVar('NRW_BSP_DEVEL', False) else '${REL_SRCREV}'}"

#BRANCH   = "${@ 'nrw/litecell15-next' if d.getVar('NRW_BSP_DEVEL', False) == "next" else 'nrw/litecell15'}"
BRANCH   = "master"
NO_SRCURI := ""
SRCURI := "git://${NRW_INTERNAL_MIRROR}/busyloop.git;protocol=ssh;branch=${BRANCH}"

SRC_URI = "${@ '${SRCURI}' if d.getVar('NRW_INTERNAL_MIRROR', False) else '${NO_SRCURI}'}"

inherit autotools pkgconfig

addtask showversion after do_compile before do_install
do_showversion() {
    bbplain "${PN}: ${PKGGITV} => ${BRANCH}:${PKGGITH}"
}

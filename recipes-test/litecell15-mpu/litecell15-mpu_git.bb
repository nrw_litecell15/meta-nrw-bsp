SUMMARY = "LiteCell 1.5 Test Application (MPU)"
LICENSE = "CLOSED"

NRW_INTERNAL_MIRROR ??= ""

DEPENDS = "lc15-firmware"
RDEPENDS_${PN} = "libgps"

S = "${WORKDIR}/git"

inherit gitver-pkg gitver-repo systemd

REPODIR   = "${THISDIR}"
REPOFILE  = "litecell15-mpu_git.bb"
PR       := "r${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

PV   = "git${SRCPV}" 
PKGV = "${PKGGITV}"
    
DEV_SRCREV  = "${AUTOREV}"
REL_SRCREV  = "5e126394e57fca275ab6aeda4616c31744261ca8"
SRCREV      = "${@ '${DEV_SRCREV}' if d.getVar('NRW_BSP_DEVEL', False) else '${REL_SRCREV}'}"

BRANCH   = "${@ 'nrw/litecell15-next' if d.getVar('NRW_BSP_DEVEL', False) == "next" else 'nrw/litecell15'}"
NO_SRCURI := ""
SRCURI := "git://${NRW_INTERNAL_MIRROR}/litecell15-mpu.git;protocol=ssh;branch=${BRANCH}"

SRCURI += "file://mpu.service"

SRC_URI = "${@ '${SRCURI}' if d.getVar('NRW_INTERNAL_MIRROR', False) else '${NO_SRCURI}'}"

inherit autotools pkgconfig

addtask showversion after do_compile before do_install
do_showversion() {
    bbplain "${PN}: ${PKGGITV} => ${BRANCH}:${PKGGITH}"
}

do_install_append() {
	# install systemd
	install -d ${D}${systemd_unitdir}/system
	install -d ${D}${systemd_unitdir}/system/multi-user.target.wants/
	install -m 0644 ${WORKDIR}/mpu.service ${D}${systemd_unitdir}/system/
       # enable systemd on sysinit
       ln -sf ../mpu.service ${D}${systemd_unitdir}/system/multi-user.target.wants/
}


#PACKAGES =+ "litecell15-mpu-service"
FILES_${PN}= " ${systemd_unitdir}/system/mpu.service \
	      ${systemd_unitdir}/system/multi-user.target.wants/mpu.service \
               /usr/bin/litecell15_mpu \
              "	









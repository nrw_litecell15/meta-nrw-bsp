inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "openssh_%.bbappend"
PR       := "${PR}.${REPOGITFN}"

do_install_append() {
            ln -sf /mnt/rom/user/config/ssh/ssh_host_rsa_key ${D}${sysconfdir}/ssh/ssh_host_rsa_key
            ln -sf /mnt/rom/user/config/ssh/ssh_host_rsa_key.pub ${D}${sysconfdir}/ssh/ssh_host_rsa_key.pub
            # Replaces original config files for read-only rootfs
            rm -f ${D}${sysconfdir}/ssh/sshd_config_readonly
            install -m 644 ${D}${sysconfdir}/ssh/sshd_config ${D}${sysconfdir}/ssh/sshd_config_readonly
            sed -i '/HostKey/d' ${D}${sysconfdir}/ssh/sshd_config_readonly
            echo "HostKey /etc/ssh/ssh_host_rsa_key" >> ${D}${sysconfdir}/ssh/sshd_config_readonly
            echo "HostKey /var/run/ssh/ssh_host_rsa_key" >> ${D}${sysconfdir}/ssh/sshd_config_readonly
            sed -i '/PermitRootLogin/d' ${D}${sysconfdir}/ssh/sshd_config_readonly
            echo "PermitRootLogin no" >> ${D}${sysconfdir}/ssh/sshd_config_readonly
            sed -i '/sftp-server/d' ${D}${sysconfdir}/ssh/sshd_config_readonly
            sed -i '/AuthorizedKeysFile/d' ${D}${sysconfdir}/ssh/sshd_config_readonly
            echo "AuthorizedKeysFile /etc/ssh/authorized_keys" >> ${D}${sysconfdir}/ssh/sshd_config_readonly
            ln -sf /mnt/rom/user/config/ssh/authorized_keys ${D}${sysconfdir}/ssh/authorized_keys
            install -d 755 ${D}/home/root/.ssh
            install -d 755 ${D}/home/gsm/.ssh
            sed -i '/StrictHostKeyChecking/d' ${D}${sysconfdir}/ssh/ssh_config
            echo "StrictHostKeyChecking no" >> ${D}${sysconfdir}/ssh/ssh_config
            sed -i '/UserKnownHostsFile/d' ${D}${sysconfdir}/ssh/ssh_config
            echo "UserKnownHostsFile /dev/null" >> ${D}${sysconfdir}/ssh/ssh_config
            sed -i '/IdentityFile/d' ${D}${sysconfdir}/ssh/ssh_config
            echo "IdentityFile /etc/ssh/ssh_host_rsa_key" >> ${D}${sysconfdir}/ssh/ssh_config
            echo "IdentityFile /var/run/ssh/ssh_host_rsa_key" >> ${D}${sysconfdir}/ssh/ssh_config
            sed -i '/ForwardAgent/d' ${D}${sysconfdir}/ssh/ssh_config
            echo "#   ForwardAgent no" >> ${D}${sysconfdir}/ssh/ssh_config
            sed -i '/ForwardX11/d' ${D}${sysconfdir}/ssh/ssh_config
            echo "#   ForwardX11 no" >> ${D}${sysconfdir}/ssh/ssh_config
            # Allows customization of ssh/sshd
            install -d 755 ${D}${sysconfdir}/defaultconfig
            install -d 755 ${D}${sysconfdir}/defaultconfig/config
            install -d 755 ${D}${sysconfdir}/defaultconfig/config/ssh
            install -m 0644 ${D}${sysconfdir}/ssh/ssh_config ${D}${sysconfdir}/defaultconfig/config/ssh/ssh_config
            rm -rf ${D}${sysconfdir}/ssh/ssh_config
            ln -sf /mnt/rom/user/config/ssh/ssh_config ${D}${sysconfdir}/ssh/ssh_config
            install -m 0644 ${D}${sysconfdir}/ssh/sshd_config_readonly ${D}${sysconfdir}/defaultconfig/config/ssh/sshd_config_readonly
            rm -rf ${D}${sysconfdir}/ssh/sshd_config_readonly
            ln -sf /mnt/rom/user/config/ssh/sshd_config_readonly ${D}${sysconfdir}/ssh/sshd_config_readonly
}

FILES_${PN} += "/home/root/.ssh \
            /home/gsm/.ssh \
            ${sysconfdir}/ssh/ssh_host_rsa_key.pub"
FILES_${PN}-sshd += "${sysconfdir}/defaultconfig/config/ssh/sshd_config_readonly \
            "
FILES_${PN}-ssh += "${sysconfdir}/defaultconfig/config/ssh/ssh_config \
            "

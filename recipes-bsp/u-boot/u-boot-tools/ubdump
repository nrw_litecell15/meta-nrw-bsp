#!/bin/sh

# U-Boot data size is located at offset 12 in the programmed image
UBOOT_SIZE_OFFT="12"
UBOOT_SIZE_MAX="524288" # 512KiB
INPUT=$1
OUTPUT=$2

if [ "$#" != 2 ]; then
    echo "Usage: ubdump <SRC(mtdblock device)> <DEST>"
    echo ""
    echo "Example: ubdump /dev/mtdblock4 /tmp/u-boot.img"
    exit 10
fi
if ! echo ${INPUT} | grep -q "mtdblock[0-9]"; then
    echo >&2 "Error: use the mtd block device"
    exit 12
fi
# Read u-boot size from image header (4 bytes at offset 12)
dd bs=1 count=4 skip=12 if=${INPUT} of=${OUTPUT} 2>/dev/null

DATA_SIZE=0
while read b0 b1 b2 b3
do
    DATA_SIZE=$((0x${b0}${b1}${b2}${b3}))
    break
done <<EOF
$(hexdump -ve '1/1 "%.2x "' ${OUTPUT})
EOF

TOTAL_SIZE=$((${DATA_SIZE}+64))

echo "Data Size:  $DATA_SIZE"
echo "Total Size: $TOTAL_SIZE"

if [ "$DATA_SIZE" -gt "$UBOOT_SIZE_MAX" ]; then
    echo >&2 "Error: size looks bad, ${DATA_SIZE} bytes."
    exit 11
fi

# Read complete u-boot image
dd bs=${TOTAL_SIZE} count=1 if=${INPUT} of=${OUTPUT} 2>/dev/null

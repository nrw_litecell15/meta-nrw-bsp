#!/bin/sh

MLO_HEADER_SIZE=512
MLO_HEADER_TOTAL_SIZE=520
# Checksum is 128 bits == 16 bytes
CHECKSUM_SIZE=16
# Max MLO file size = total partition size minus MLO header size with checksum:
# 128*1024 (128K) - 520 - 16
MAX_SIZE=$((128*1024 - $MLO_HEADER_TOTAL_SIZE - $CHECKSUM_SIZE))
INPUT=$1
OUTPUT=$2

if [ "$#" != 2 ]; then
    echo "Usage: mlodump <SRC(mtdblock device)> <DEST>"
    echo ""
    echo "Example: mlodump /dev/mtdblock0 /tmp/MLO"
    exit 10
fi
if ! echo ${INPUT} | grep -q "mtdblock[0-9]"; then
    echo >&2 "Error: use the mtd block device"
    exit 12
fi
echo "$INPUT read size from header"
size_hex_low=`hexdump $INPUT -s $MLO_HEADER_SIZE -n 2 | head -n 1 | cut -d ' ' -f 2`
size_hex_high=`hexdump $INPUT -s $((MLO_HEADER_SIZE + 2)) -n 2 | head -n 1 | cut -d ' ' -f 2`
size_hex=$size_hex_high$size_hex_low
size=`printf "%d" 0x$size_hex`
echo "$INPUT size is $size"
if [ $size -gt $MAX_SIZE ] || [ $size -le 0 ]; then
    echo "* $INPUT size header is corrupted!"
    exit 11
fi
TOTAL_SIZE=$(($size+$MLO_HEADER_TOTAL_SIZE+$CHECKSUM_SIZE))
echo "$INPUT read size is $TOTAL_SIZE"
# Read complete MLO image
dd bs=${TOTAL_SIZE} count=1 if=${INPUT} of=${OUTPUT} 2>/dev/null
